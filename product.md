---
layout: product
title: Products
permalink: /product/
---

We have 5'×5' deer stands for sale. They are one piece molded plastic with almost 360° field of view. The stands are portable and easy to move with the attached wheel assembly. The opening is 3'w X 4'h that swings out with the window that measures 9"h x 24"w. The other three sides have windows that measures 12"h x 48"w. All windows will hinge up. Over 6' of standing room. Four wheeler and propane assembly not included. $1895 plus tax. Delivery is negotiable. Thanks for looking
