---
layout: contact
title: Contact Us
permalink: /contact/
---

Interested in purchasing a new deer stand? Contact us here for more information!

Otherwise, contact Dave Thompson directly at 701-261-5546
